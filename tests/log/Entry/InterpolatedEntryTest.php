<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;
use mef\Log\Entry\InterpolatedEntry;
use mef\Log\Processor\NullProcessor;

use mef\Stringifier\Stringifier;
use mef\StringInterpolation\PlaceholderInterpolator;

class InterpolatedEntryTest extends MefworksTestCase
{
	protected $interpolator;

	public function setUp() : void
	{
		parent::setUp();

		$this->interpolator = new PlaceholderInterpolator(new Stringifier);
	}

	public function testSimpleProperties()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$append = 'Append';

		$entry = new InterpolatedEntry($dt, $level, $message, [], $this->interpolator);

		$this->assertEquals($dt, $entry->getDateTime());
		$this->assertSame($level, $entry->getLevel());
	}

	public function testContext()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new InterpolatedEntry($dt, $level, $message, $context, $this->interpolator);

		$this->assertEquals($context, $entry->getContext());
	}

	public function testProcessor()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new InterpolatedEntry($dt, $level, $message, $context, $this->interpolator);

		$this->assertEquals($context, $entry->getContext());
	}

	public function testInterpolator()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new InterpolatedEntry(
			$dt, $level, $message, $context,
			$this->interpolator
		);

		$this->assertEquals($message, $entry->getMessage());
	}

	public function testToString()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new InterpolatedEntry(
			$dt, $level, $message, $context,
			$this->interpolator
		);

		$this->assertTrue(is_string((string) $entry));
	}
}