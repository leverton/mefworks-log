<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use mef\Log\Entry\EntryFactory;

class EntryFactoryTest extends MefworksTestCase
{
	public function testNoInterpolation()
	{
		$factory = new EntryFactory;

		$this->assertNull($factory->getStringInterpolator());
	}

	public function testInterpolationSetter()
	{
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator(new mef\Stringifier\Stringifier);
		$factory = new EntryFactory;
		$factory->setStringInterpolator($interpolator);

		$this->assertSame($interpolator, $factory->getStringInterpolator());

		$factory->unsetStringInterpolator();
		$this->assertNull($factory->getStringInterpolator());
	}

	public function testProcessor()
	{
		$factory = new EntryFactory;

		$processor = new mef\Log\Processor\NullProcessor;

		$factory->setProcessor($processor);
		$this->assertEquals($processor, $factory->getProcessor());

		$factory->unsetProcessor();
		$this->assertNull($factory->getProcessor());
	}

	public function testDefaultTimeZone()
	{
		$factory = new EntryFactory;

		$this->assertSame('UTC', $factory->getTimeZone()->getName());
	}

	public function testUpdateTimeZone()
	{
		$factory = new EntryFactory;

		$tz = new DateTimeZone('America/Chicago');

		$factory->setTimeZone($tz);

		$this->assertSame($tz, $factory->getTimeZone());
	}
}