<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class ChainProcessorTest extends MefworksTestCase
{
	public function testBasic()
	{
		$message = 'Hello, World!';

		$chainProcessor = new \mef\Log\Processor\ChainProcessor;
		$chainProcessor->addProcessor(new \mef\Log\Processor\NullProcessor);

		$entry = new mef\Log\Entry\Entry(
			new DateTimeImmutable,
			LogLevel::INFO,
			$message,
			[]
		);

		$entry = $chainProcessor->process($entry);

		$this->assertEquals($entry->getMessage(), $message);
	}
}