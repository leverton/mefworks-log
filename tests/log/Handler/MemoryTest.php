<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

class MemoryTest extends MefworksTestCase
{
	public function testEmpty()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;

		$this->assertEquals(0, count($memoryHandler));
		$this->assertNull($memoryHandler->dequeue());
	}

	public function testClear()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;
		$logger = new mef\Log\StandardLogger($memoryHandler);

		$logger->info('a');
		$this->assertEquals(1, count($memoryHandler));

		$memoryHandler->clear();
		$this->assertEquals(0, count($memoryHandler));
	}

	public function testIterator()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;
		$logger = new mef\Log\StandardLogger($memoryHandler);

		$logger->info('a');

		foreach ($memoryHandler as $logEntry)
		{
			$this->assertTrue($logEntry instanceof mef\Log\Entry\EntryInterface);
		}
	}
}