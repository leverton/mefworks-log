<?php namespace mef\Log\Handler;

require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class OutputTest extends \MefworksTestCase
{
	public function testOutput()
	{
		$outputHandler = new \mef\Log\Handler\OutputHandler;

		$outputHandler->setLogEntryFormatter(new \mef\Log\Formatter\CallbackFormatter(
			function(\mef\Log\Entry\EntryInterface $logEntry) {
				return $logEntry->getMessage();
			}
		));

		$logger = new \mef\Log\StandardLogger($outputHandler);

		ob_start();
		$logger->info('Hello, World!');
		$output = ob_get_clean();

		$this->assertEquals('Hello, World!' . PHP_EOL, $output);
	}
}