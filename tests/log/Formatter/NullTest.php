<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

class NullTest extends MefworksTestCase
{
	public function testNull()
	{
		$formatter = new mef\Log\Formatter\NullFormatter;
		$memoryHandler = new mef\Log\Handler\MemoryHandler;

		$logger = new mef\Log\StandardLogger($memoryHandler);
		$logger->info('abcdef');

		$logEntry = $memoryHandler->dequeue();
		$output = $formatter->formatLogEntry($logEntry);

		$this->assertNull($output);
	}
}