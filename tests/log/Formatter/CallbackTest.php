<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

class CallbackTest extends MefworksTestCase
{
	public function testCallback()
	{
		$formatter = new mef\Log\Formatter\CallbackFormatter(function(mef\Log\Entry\EntryInterface $entry)
		{
			return strtoupper($entry->getMessage());
		});

		$memoryHandler = new mef\Log\Handler\MemoryHandler;

		$logger = new mef\Log\StandardLogger($memoryHandler);
		$logger->info('abcdef');

		$logEntry = $memoryHandler->dequeue();
		$output = $formatter->formatLogEntry($logEntry);

		$this->assertEquals('ABCDEF', $output);
	}
}