<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

class FormatterAwareTraitTest extends MefworksTestCase
{
	public function testFormatterAwareTrait()
	{
		$mock = new MockFormatterAware;

		$this->assertNull($mock->getLogEntryFormatter());

		$nullFormatter = new mef\Log\Formatter\NullFormatter;
		$mock->setLogEntryFormatter($nullFormatter);

		$this->assertEquals($nullFormatter, $mock->getLogEntryFormatter());
	}
}

class MockFormatterAware
{
	use mef\Log\Formatter\FormatterAwareTrait;
}