<?php namespace mef\Log\Example;

require_once __DIR__ . '/../vendor/autoload.php';

// Sets up a logger that sends logs to stderr and stdout, depending on the
// severity. This can be overridden by passing a different handler to the
// constructor.
//
// A placeholder string interpolator is set up so that values from the context
// can be injected into the message.
//
$logger = new \mef\Log\StandardLogger;

// By default the logger uses the UTC time zone.
$logger->getLogEntryFactory()->setTimeZone(new \DateTimeZone(date_default_timezone_get()));

// The interpolation can be either index based or key based.
$logger->notice('Hello, {0} {1}!', ['John', 'Doe']);
$logger->warning('And this goes to stderr {microtime}', ['microtime' => microtime(true)]);