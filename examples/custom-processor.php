<?php namespace mef\Log\Example;

require_once __DIR__ . '/../vendor/autoload.php';

// Custom handlers need to implement the \mef\Log\Handler\HandlerInterface,
// which defines two methods: handleLogEntry and handleLogEntryBatch. In most
// cases, extending the AbstractHandler is appropriate.
//
// It implements a default batch method that calls the handleLogEntry for each
// entry. It also uses the \mef\Log\FilterTrait, which adds the capability to
// filter out events by log level.
//
// This handler simply associates the logged entry with the $lastEntry instance
// variable.
//
class MyProcessor implements \mef\Log\Processor\ProcessorInterface
{
	public function process(\mef\Log\Entry\EntryInterface $entry) : \mef\Log\Entry\EntryInterface
	{
		return $entry->withMessage(strtoupper($entry->getMessage()))->withAddedContext(['processed' => true]);
	}
}

$myProcessor = new MyProcessor;

$logger = new \mef\Log\StandardLogger;
$logger->getLogEntryFactory()->setProcessor($myProcessor);
$logger->info('Hello, World!', ['processed' => false]);

// The above message will be processed before it shows up on stdout ... so
// the message be displayed in upper case letters.