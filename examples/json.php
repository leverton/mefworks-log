<?php namespace mef\Log\Example;

require_once __DIR__ . '/../vendor/autoload.php';

/**
 * If a class implements JsonSerializable, then it will be used when
 * it is referenced by a log's $context.
 */
class MyTestClass implements \JsonSerializable
{
	public function jsonSerialize()
	{
		return 'Created By jsonSerialize';
	}
}

// Set up a logger that sends logs to stdout as JSON
$formatter = \mef\Log\Formatter\JsonFormatter::withDefaultStringifier();
$stdoutHandler = new \mef\Log\Handler\StdoutHandler($formatter);
$logger = new \mef\Log\StandardLogger($stdoutHandler);

// The interpolation can be either index based or key based.
$context = [
	'first_name' => 'John',
	'last_name' => 'Doe',
	'dates' => [
		new \DateTimeImmutable('yesterday'),
		new \DateTimeImmutable('today'),
		new \DateTimeImmutable('tomorrow'),
	],
	'custom' => new MyTestClass,
	'foobar'
];
$logger->notice('Hello, {first_name} {last_name}!', $context);