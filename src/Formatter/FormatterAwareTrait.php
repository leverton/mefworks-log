<?php namespace mef\Log\Formatter;

/**
 * Provide getters and setters for a mef\Log\Formatter\FormatterInterface
 * object backed by the protected variable $logEntryFormatter.
 */
trait FormatterAwareTrait
{
	/**
	 * @var \mef\Log\Formatter\FormatterInterface
	 */
	protected $logEntryFormatter;

	/**
	 * Set the current log entry formatter.
	 *
	 * @param \mef\Log\Formatter\FormatterInterface $logEntryFormatter
	 */
	public function setLogEntryFormatter(FormatterInterface $logEntryFormatter)
	{
		$this->logEntryFormatter = $logEntryFormatter;
	}

	/**
	 * Return the current log entry formatter.
	 *
	 * @return \mef\Log\Formatter\FormatterInterface
	 */
	public function getLogEntryFormatter()
	{
		return $this->logEntryFormatter;
	}
}