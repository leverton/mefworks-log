<?php namespace mef\Log\Formatter;

use JsonSerializable;
use Traversable;

use mef\Log\Entry\EntryInterface;

use mef\Stringifier\Stringifier;
use mef\Stringifier\StringifierAwareTrait;
use mef\Stringifier\StringifierAwareInterface;

/**
 * Formats the entry as a JSON string.
 */
class JsonFormatter implements FormatterInterface, StringifierAwareInterface
{
	use StringifierAwareTrait;

	/**
	 * Constructor.
	 *
	 * @param \mef\Stringifier\Stringifier $stringifier
	 */
	public function __construct(Stringifier $stringifier)
	{
		$this->stringifier = $stringifier;
	}

	/**
	 * Set up the JSON string formatter with reasonable defaults.
	 *
	 * @return \mef\Log\Formatter\JsonFormatter $stringifier
	 */
	public static function withDefaultStringifier()
	{
		return new self(new Stringifier);
	}

	/**
	 * Format the $entry as a JSON string.
	 *
	 * @param  \mef\Log\Entry\EntryInterface $entry
	 *
	 * @return string
	 */
	public function formatLogEntry(EntryInterface $entry)
	{
		$context = $this->makeJsonFriendly($entry->getContext());

		$indexedContext = [];

		foreach ($context as $key => $value)
		{
			if (is_integer($key) === true)
			{
				$indexedContext[] = $value;
				unset($context[$key]);
			}
		}

		$data = [
			'timestamp' => $entry->getDateTime()->format(\DateTime::W3C),
			'level' => $entry->getLevel(),
			'message' => $entry->getMessage(),
		] + $context;

		if ($indexedContext !== [])
		{
			$data['args'] = $indexedContext;
		}

		return json_encode($data);
	}

	/**
	 * Make the context something that can be printed nicely via json_encode.
	 *
	 * @param  mixed  $context
	 *
	 * @return mixed
	 */
	private function makeJsonFriendly($context)
	{
		if ($context instanceof JsonSerializable)
		{
			// If the object implements JsonSerializable, then allow it to do
			// its job.
			return $context;
		}
		else if (is_array($context))
		{
			// Recursively map arrays
			return array_map([$this, 'makeJsonFriendly'], $context);
		}
		else if ($context instanceof Traversable)
		{
			// Recursively map anything that is traversable
			$output = [];
			foreach ($context as $key => $value)
			{
				$output[$key] = $this->makeJsonFriendly($value);
			}
			return $output;
		}
		else
		{
			// Otherwise convert the value into a string, since the output of
			// json_encode() on arbitrary things can be problematic.
			return $this->stringifier->stringify($context);
		}
	}
}