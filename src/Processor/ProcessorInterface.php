<?php namespace mef\Log\Processor;

use mef\Log\Entry\EntryInterface;

/**
 * An object that implements this interface is able to add additional data
 * to a log.
 *
 * This could include thing like the user's IP address or the memory usage
 * statistics.
 */
interface ProcessorInterface
{
	/**
	 * Process the log entry.
	 *
	 * Both the message and the context can be modified at the processor's
	 * discretion. The log level may not be modified.
	 *
	 * @param \mef\Log\Entry\EntryInterface $entry
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function process(EntryInterface $entry) : EntryInterface;
}