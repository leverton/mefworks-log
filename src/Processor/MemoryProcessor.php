<?php namespace mef\Log\Processor;

use mef\Log\Entry\EntryInterface;

/**
 * Add memory usage details to the log entry.
 */
class MemoryProcessor implements ProcessorInterface
{
	/**
	 * Add memory usage details to the log entry.
	 *
	 * @param \mef\Log\Entry\EntryInterface $entry
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function process(EntryInterface $entry) : EntryInterface
	{
		return $entry->withAddedContext(['memory_usage' => memory_get_usage()]);
	}
}
