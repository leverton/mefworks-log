<?php namespace mef\Log\Processor;

use mef\Log\Entry\EntryInterface;

/**
 * An empty implementation of a ProcessorInterface.
 */
class NullProcessor implements ProcessorInterface
{
	/**
	 * Does nothing.
	 *
	 * @param \mef\Log\Entry\MutableEntryInterface $entry
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function process(EntryInterface $entry) : EntryInterface
	{
		return $entry;
	}
}