<?php namespace mef\Log\Processor;

use mef\Log\Entry\EntryInterface;

/**
 * Used to chain multiple processors together so that multiple processors
 * can process the same message.
 */
class ChainProcessor implements ProcessorInterface
{
	/**
	 * The list of processors.
	 *
	 * @var array
	 */
	private $processors = [];

	/**
	 * Add a processor to the end of the list.
	 *
	 * @param \mef\Log\Processor\ProcessorInterface $processor
	 */
	public function addProcessor(ProcessorInterface $processor)
	{
		$this->processors[] = $processor;
	}

	/**
	 * Iterate over each attached processor in succession.
	 *
	 * @param \mef\Log\Entry\EntryInterface $entry
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function process(EntryInterface $entry) : EntryInterface
	{
		foreach ($this->processors as $processor)
		{
			$entry = $processor->process($entry);
		}

		return $entry;
	}
}
