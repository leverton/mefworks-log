<?php namespace mef\Log\Processor;

interface ProcessorAwareInterface
{
	/**
	 * Return the current processor.
	 *
	 * @return \mef\Log\Processor\ProcessorInterface
	 */
	public function getProcessor();

	/**
	 * Set the processor for future log entries.
	 *
	 * @param \mef\Log\Processor\ProcessorInterface $processor
	 */
	public function setProcessor(ProcessorInterface $processor = null);
}