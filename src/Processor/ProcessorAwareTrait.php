<?php namespace mef\Log\Processor;

trait ProcessorAwareTrait
{
	/**
	 * @var \mef\Log\Processor\ProcessorInterface
	 */
	protected $processor;

	/**
	 * Return the current processor.
	 *
	 * @return \mef\Log\Processor\ProcessorInterface
	 */
	public function getProcessor()
	{
		return $this->processor;
	}

	/**
	 * Set the processor for future log entries.
	 *
	 * @param \mef\Log\Processor\ProcessorInterface $processor
	 */
	public function setProcessor(ProcessorInterface $processor = null)
	{
		$this->processor = $processor;
	}
}