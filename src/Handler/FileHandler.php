<?php namespace mef\Log\Handler;

use mef\Log\Formatter\FormatterInterface;

/**
 * Logs to a file.
 */
class FileHandler extends StreamHandler
{
	/**
	 * The file pointer to the log file.
	 *
	 * @var resource
	 */
	private $fp;

	/**
	 * The constructor.
	 *
	 * @param string $path   the path to the file
	 * @param string $mode   the file mode, defaults to 'a' (see fopen)
	 * @param \mef\Log\Formatter\FormatterInterface $formatter
	 */
	public function __construct($path, $mode = 'a', FormatterInterface $formatter = null)
	{
		$this->fp = fopen($path, $mode);
		parent::__construct($this->fp, $formatter);
	}

	/**
	 * The destructor.
	 *
	 * Close the file.
	 */
	public function __destruct()
	{
		fclose($this->fp);
	}
}