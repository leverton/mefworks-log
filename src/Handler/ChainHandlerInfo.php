<?php namespace mef\Log\Handler;

/**
 * A private class used by the ChainHandler.
 *
 * This should not be used in userland code.
 */
final class ChainHandlerInfo
{
	/**
	 * @var \mef\Log\Handler\HandlerInterface
	 */
	public $handler;

	/**
	 * @var boolean
	 */
	public $stopOnConsume;

	/**
	 * Constructor
	 *
	 * @param \mef\Log\Handler\HandlerInterface $handler
	 * @param boolean $stopOnConsume
	 */
	final public function __construct(HandlerInterface $handler, $stopOnConsume)
	{
		$this->handler = $handler;
		$this->stopOnConsume = (bool) $stopOnConsume;
	}
}