<?php namespace mef\Log\Handler;

trait HandlerAwareTrait
{
	/**
	 * @var \mef\Log\Handler\HandlerInterface
	 */
	protected $handler;

	/**
	 * Returns the current handler.
	 *
	 * @return \mef\Log\Handler\HandlerInterface
	 */
	public function getHandler()
	{
		return $this->handler;
	}

	/**
	 * Set the handler that will process future log entries.
	 *
	 * To set multiple handlers, use a ChainHandler.
	 *
	 * @param \mef\Log\Handler\HandlerInterface $handler
	 */
	public function setHandler(HandlerInterface $handler)
	{
		$this->handler = $handler;
	}
}