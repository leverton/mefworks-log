<?php namespace mef\Log\Handler;

use Traversable;

use mef\Log\Entry\EntryInterface;
use mef\Log\FilterTrait;

/**
 * This can serve as a base to most handlers.
 *
 * Note that handleLogEntry will need to respect the FilterTrait settings
 * by checking $this->willHandleLevel($entry->getLevel()).
 */
abstract class AbstractHandler implements HandlerInterface
{
	use FilterTrait;

	abstract public function handleLogEntry(EntryInterface $entry);

	/**
	 * Process each entry one by one.
	 *
	 * @param \Traversable $entries
	 */
	public function handleLogEntryBatch(Traversable $entries)
	{
		foreach ($entries as $entry)
		{
			$this->handleLogEntry($entry);
		}
	}
}