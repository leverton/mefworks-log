<?php namespace mef\Log\Handler;

use mef\Log\Formatter\FormatterInterface;

/**
 * Log to PHP's output buffer.
 *
 * This is equivalent to logging via 'echo'.
 */
class OutputHandler extends StreamHandler
{
	/**
	 * Constructor
	 *
	 * Equivalent to opening the 'php://output' file.
	 *
	 * @param \mef\Log\Formatter\FormatterInterface $formatter
	 */
	public function __construct(FormatterInterface $formatter = null)
	{
		parent::__construct(fopen('php://output', 'w'), $formatter);
	}
}