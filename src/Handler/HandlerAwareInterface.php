<?php namespace mef\Log\Handler;

interface HandlerAwareInterface
{
	/**
	 * Returns the current handler.
	 *
	 * @return \mef\Log\Handler\HandlerInterface
	 */
	public function getHandler();

	/**
	 * Set the handler that will process future log entries.
	 *
	 * To set multiple handlers, use a ChainHandler.
	 *
	 * @param \mef\Log\Handler\HandlerInterface $handler
	 */
	public function setHandler(HandlerInterface $handler);
}