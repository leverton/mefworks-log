<?php namespace mef\Log\Handler;

use mef\Log\Formatter\FormatterInterface;

/**
 * Log to stderr.
 */
class StderrHandler extends AbstractStdHandler 
{
	/**
	 * Constructor
	 *
	 * Uses the predefined STDERR file handler if available; otherwise opens
	 * the 'php://stderr' file.
	 *
	 * @param \mef\Log\Formatter\FormatterInterface $formatter
	 */
	public function __construct(FormatterInterface $formatter = null)
	{
		parent::__construct('STDERR', 'php://stderr', $formatter);
	}
}
