<?php namespace mef\Log\Handler;

use Countable;
use IteratorAggregate;
use RuntimeException;
use SplQueue;

use mef\Log\Entry\EntryInterface;
use Traversable;

/**
 * Store the log entries in memory.
 *
 * The entries are stored as \mef\Log\EntryInterface objects.
 */
class MemoryHandler extends AbstractHandler implements IteratorAggregate, Countable
{
	/**
	 * The list of logged entries.
	 *
	 * @var SplQueue
	 */
	private $entries;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->entries = new SplQueue();
	}

	/**
	 * Add the log entry to memory.
	 *
	 * @param  mef\Log\EntryInterface $entry
	 *
	 * @return boolean  true if consumed
	 */
	public function handleLogEntry(EntryInterface $entry)
	{
		if ($this->willHandleLevel($entry->getLevel()) === false)
		{
			return false;
		}

		$this->entries->enqueue($entry);
		return true;
	}

	/**
	 * Return an iterator that iterate over all log entries from oldest to
	 * newest.
	 *
	 * @return \Iterator
	 */
	public function getIterator(): Traversable
	{
		return $this->entries;
	}

	/**
	 * Return an array of all entries.
	 *
	 * To iterate over the log entries, simply use foreach over the object.
	 * <code>
	 * foreach ($memoryHandler as $logEntry) { ... }
	 * </code>
	 *
	 * @return array
	 */
	public function getEntries()
	{
		return iterator_to_array($this->entries);
	}

	/**
	 * Remove and return the oldest entry.
	 *
	 * Return null if there are no entries.
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function dequeue()
	{
		try
		{
			return $this->entries->dequeue();
		}
		catch (RuntimeException $e)
		{
			return null;
		}
	}

	/**
	 * Remove all entries from memory.
	 */
	public function clear()
	{
		$this->entries = new SplQueue();
	}

	/**
	 * Return the number of logged entries.
	 *
	 * @return integer
	 */
	public function count(): int
	{
		return count($this->entries);
	}
}