<?php namespace mef\Log;

interface LoggerAwareInterface
{
	/**
	 * Return the current logger.
	 *
	 * @return \mef\Log\Logger
	 */
	public function getLogger();

	/**
	 * Set the current logger.
	 *
	 * @param \mef\Log\Logger $logger
	 */
	public function setLogger(Logger $logger);
}