<?php namespace mef\Log\Entry;

trait EntryToStringTrait
{
	/**
	 * Return the time of when the log entry was created.
	 *
	 * @return \DateTimeInterface
	 */
	abstract public function getDateTime();

	/**
	 * Return the severity level.
	 *
	 * @return string
	 */
	abstract public function getLevel();

	/**
	 * Return the message.
	 *
	 * @return string
	 */
	abstract public function getMessage();

	/**
	 * Return the extra context data.
	 *
	 * @return array
	 */
	abstract public function getContext();

	/**
	 * Return all data as a string.
	 *
	 * @return string
	 */
	public function __toString()
	{
		$string = $this->getDateTime()->format('Y-m-d H:i:s') . ' [' . $this->getLevel() . '] ' . $this->getMessage();

		$context = $this->getContext();

		if ($context !== [])
		{
			$string .= ' ' . print_r($context, true);
		}

		return $string;
	}
}