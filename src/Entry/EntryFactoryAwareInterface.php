<?php namespace mef\Log\Entry;

interface EntryFactoryAwareInterface
{
	/**
	 * Return the log entry factory
	 *
	 * @param \mef\Log\Entry\EntryFactoryInterface
	 */
	public function getLogEntryFactory();

	/**
	 * Set the log entry factory.
	 *
	 * @param \mef\Log\Entry\EntryFactoryInterface
	 */
	public function setLogEntryFactory(EntryFactoryInterface $logEntryFactory);
}