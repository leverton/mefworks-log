<?php namespace mef\Log\Entry;

/**
 * An interface that represents a log item.
 *
 * \mef\Log\Entry implements this interface.
 */
interface EntryInterface
{
	/**
	 * Return the time of when the log entry was created.
	 *
	 * @return \DateTimeInterface
	 */
	public function getDateTime();

	/**
	 * Return the severity level.
	 *
	 * @return string
	 */
	public function getLevel();

	/**
	 * Return the message.
	 *
	 * @return string
	 */
	public function getMessage();

	/**
	 * Return the extra context data.
	 *
	 * @return array
	 */
	public function getContext();

	/**
	 * Return a new instance with a different message.
	 *
	 * @param  string $message  The new message.
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function withMessage(string $message) : EntryInterface;

	/**
	 * Return a new instance with additional context.
	 *
	 * @param  array  $context The context to add.
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function withAddedContext(array $context) : EntryInterface;

	/**
	 * Return a string representation.
	 *
	 * @return string
	 */
	public function __toString();
}