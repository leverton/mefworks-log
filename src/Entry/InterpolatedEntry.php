<?php namespace mef\Log\Entry;

use DateTimeInterface;

use mef\StringInterpolation\StringInterpolatorInterface;

/**
 * A log entry that has its processing deferred until after one if its
 * properties is accessed.
 *
 * Normally there is no need to create one of these objects. It is the
 * responsibility of the Logger to create one and pass it to the Handler.
 *
 * The object is immutable.
 */
class InterpolatedEntry extends Entry
{
	use EntryToStringTrait;

	/**
	 * The original context prior to any interpolation.
	 *
	 * @var array
	 */
	private $fullContext;

	/**
	 * The interpolator that applies the context to the message.
	 *
	 * @var \mef\StringInterpolation\StringInterpolatorInterface
	 */
	private $interpolator;

	/**
	 * Set to true if the message has been processed.
	 *
	 * @var boolean
	 */
	private $isProcessed = false;

	/**
	 * Constructor.
	 *
	 * @param \DateTimeInterface $dateTime          time of log
	 * @param string             $level             a Psr\Log\LogLevel constant
	 * @param string             $message           the message to log
	 * @param array              $context           extra data
	 * @param \mef\StringInterpolation\StringInterpolatorInterface $interpolator
	 */
	public function __construct(DateTimeInterface $dateTime, $level, $message,
		array $context, StringInterpolatorInterface $interpolator)
	{
		$this->dateTime = $dateTime;
		$this->level = (string) $level;
		$this->message = (string) $message;
		$this->context = $context;
		$this->fullContext = $context;
		$this->interpolator = $interpolator;
	}

	/**
	 * Return the message.
	 *
	 * @return string
	 */
	public function getMessage()
	{
		if ($this->isProcessed === false)
		{
			$this->process();
		}

		return $this->message;
	}

	/**
	 * Return the extra context data.
	 *
	 * @return array
	 */
	public function getContext()
	{
		if ($this->isProcessed === false)
		{
			$this->process();
		}

		return $this->context;
	}

	/**
	 * Return the full context data.
	 *
	 * @return array
	 */
	public function getFullContext() : array
	{
		return $this->fullContext;
	}

	/**
	 * Return a new instance with additional context.
	 *
	 * @param  array  $context The context to add.
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function withAddedContext(array $context) : EntryInterface
	{
		$entry = parent::withAddedContext($context);

		foreach ($context as $key => $value)
		{
			if ($value === null)
			{
				unset($entry->fullContext[$key]);
			}
			else
			{
				$entry->fullContext[$key] = $value;
			}
		}

		return $entry;
	}

	/**
	 * Process and interpolate the original message and context as needed.
	 */
	private function process()
	{
		$this->isProcessed = true;

		$interpolation = $this->interpolator->interpolate($this->message, $this->fullContext);
		$this->message = $interpolation->getString();
		$this->context = array_diff_key($this->fullContext, $interpolation->getUsedContext());
	}
}