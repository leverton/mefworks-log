<?php namespace mef\Log\Entry;

use DateTimeImmutable;
use DateTimeZone;

use mef\Log\Processor\ProcessorAwareInterface;
use mef\Log\Processor\ProcessorAwareTrait;
use mef\Log\Processor\ProcessorInterface;

use mef\StringInterpolation\StringInterpolatorAwareInterface;
use mef\StringInterpolation\StringInterpolatorAwareTrait;
use mef\StringInterpolation\StringInterpolatorInterface;

/**
 * Build an InterpolatedEntry or Entry object.
 */
class EntryFactory implements EntryFactoryInterface,
	ProcessorAwareInterface, StringInterpolatorAwareInterface
{
	use ProcessorAwareTrait;
	use StringInterpolatorAwareTrait;

	protected $timeZone;

	/**
	 * Constructor.
	 *
	 * @param \mef\StringInterpolation\StringInterpolatorInterface $interpolator
	 * @param \mef\Log\Processor\ProcessorInterface $processor
	 */
	public function __construct(
		StringInterpolatorInterface $interpolator = null,
		ProcessorInterface $processor = null,
		DateTimeZone $timeZone = null
	) {
		$this->stringInterpolator = $interpolator;
		$this->processor = $processor;
		$this->timeZone = $timeZone ?: new DateTimeZone('UTC');
	}

	/**
	 * Return the timezone for future log entries.
	 *
	 * @return \DateTimeZone
	 */
	public function getTimeZone()
	{
		return $this->timeZone;
	}

	/**
	 * Set the timezone for future log entries.
	 *
	 * The default timezone is UTC.
	 *
	 * @param \DateTimeZone $timeZone
	 */
	public function setTimeZone(DateTimeZone $timeZone)
	{
		$this->timeZone = $timeZone;
	}

	/**
	 * Unset the processor.
	 */
	public function unsetProcessor()
	{
		$this->processor = null;
	}

	/**
	 * Unset the string interpolator.
	 */
	public function unsetStringInterpolator()
	{
		$this->stringInterpolator = null;
	}

	/**
	 * Return an EntryInterface object.
	 *
	 * Typically only called by the Logger.
	 *
	 * @param  string $level
	 * @param  string $message
	 * @param  array $context
	 * @return \mef\Log\Entry\EntryInterface
	 * @throws \Exception
	 */
	public function createLogEntry($level, $message, array $context = [])
	{
		if ($this->stringInterpolator !== null)
		{
			$entry = new InterpolatedEntry(
				new DateTimeImmutable('', $this->timeZone),
				$level,
				$message,
				$context,
				$this->stringInterpolator
			);
		}
		else
		{
			$entry = new Entry(
				new DateTimeImmutable('', $this->timeZone),
				$level,
				$message,
				$context
			);
		}

		if ($this->processor !== null)
		{
			$entry = $this->processor->process($entry);
		}

		return $entry;
	}
}